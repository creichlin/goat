#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
export GOPATH=$(dirname $(dirname $(dirname $(dirname $DIR))))

cat tests/input.json | go run goat/main.go -template tests/tests.tpl

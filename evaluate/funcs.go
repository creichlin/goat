package evaluate

import "strings"

func split(i string) []string {
	return strings.Split(i, "\n")
}

func prefix(prefix, input string) string {
	res := []string{}
	for _, l := range strings.Split(input, "\n") {
		res = append(res, prefix + l)
	}
	return strings.Join(res, "\n")
}

func postfix(postfix, input string) string {
	res := []string{}
	for _, l := range strings.Split(input, "\n") {
		res = append(res,  l + postfix)
	}
	return strings.Join(res, "\n")
}


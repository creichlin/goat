package evaluate

import (
	"bytes"
)

// some of the code here is copied and adapted from
// https://github.com/go-yaml/yaml/
// which is under Apache2 license

func escapeYAML(i string) string {
	value := []byte(i)
	result := bytes.NewBufferString("")

	for i := 0; i < len(value); {
		if mustEscape(value, i) {
			octet := value[i]
			var w int
			var v rune
			switch {
			case octet&0x80 == 0x00:
				w, v = 1, rune(octet&0x7F)
			case octet&0xE0 == 0xC0:
				w, v = 2, rune(octet&0x1F)
			case octet&0xF0 == 0xE0:
				w, v = 3, rune(octet&0x0F)
			case octet&0xF8 == 0xF0:
				w, v = 4, rune(octet&0x07)
			}
			for k := 1; k < w; k++ {
				octet = value[i+k]
				v = (v << 6) + (rune(octet) & 0x3F)
			}
			i += w

			result.WriteByte('\\')

			switch v {
			case 0x00:
				result.WriteByte('0')
			case 0x07:
				result.WriteByte('a')
			case 0x08:
				result.WriteByte('b')
			case 0x09:
				result.WriteByte('t')
			case 0x0A:
				result.WriteByte('n')
			case 0x0b:
				result.WriteByte('v')
			case 0x0c:
				result.WriteByte('f')
			case 0x0d:
				result.WriteByte('r')
			case 0x1b:
				result.WriteByte('e')
			case 0x22:
				result.WriteByte('"')
			case 0x5c:
				result.WriteByte('\\')
			case 0x85:
				result.WriteByte('N')
			case 0xA0:
				result.WriteByte('_')
			case 0x2028:
				result.WriteByte('L')
			case 0x2029:
				result.WriteByte('P')
			default:
				if v <= 0xFF {
					result.WriteByte('x')
					w = 2
				} else if v <= 0xFFFF {
					result.WriteByte('u')
					w = 4
				} else {
					result.WriteByte('U')
					w = 8
				}
				for k := (w - 1) * 4; k >= 0; k -= 4 {
					digit := byte((v >> uint(k)) & 0x0F)
					if digit < 10 {
						result.WriteByte(digit + '0')
					} else {
						result.WriteByte(digit + 'A' - 10)
					}
				}
			}
		} else {
			w := width(value[i])
			if w < 1 || w > 4 {
				panic("invalid character width")
			}
			result.Write(value[i: i+w])
			i += w
		}
	}
	return result.String()
}

func mustEscape(b []byte, i int) bool {
	return !((b[i] == 0x0A) || // . == #x0A
		(b[i] >= 0x20 && b[i] <= 0x7E) || // #x20 <= . <= #x7E
		(b[i] == 0xC2 && b[i+1] >= 0xA0) || // #0xA0 <= . <= #xD7FF
		(b[i] > 0xC2 && b[i] < 0xED) ||
		(b[i] == 0xED && b[i+1] < 0xA0) ||
		(b[i] == 0xEE) ||
		(b[i] == 0xEF && // #xE000 <= . <= #xFFFD
			!(b[i+1] == 0xBB && b[i+2] == 0xBF) && // && . != #xFEFF
			!(b[i+1] == 0xBF && (b[i+2] == 0xBE || b[i+2] == 0xBF)))) ||
		(b[0] == 0xEF && b[1] == 0xBB && b[2] == 0xBF) || // BOM
		b[i] == '\r' || // CR (#xD)
		b[i] == '\n' || // LF (#xA)
		b[i] == 0xC2 && b[i+1] == 0x85 || // NEL (#x85)
		b[i] == 0xE2 && b[i+1] == 0x80 && b[i+2] == 0xA8 || // LS (#x2028)
		b[i] == 0xE2 && b[i+1] == 0x80 && b[i+2] == 0xA9 || // PS (#x2029)
		(b[i] == '"') ||
		(b[i] == '\\')
}

// width returns the number of bytes of the character
func width(b byte) int {
	// Don't replace these by a switch without first
	// confirming that it is being inlined.
	if b&0x80 == 0x00 {
		return 1
	}
	if b&0xE0 == 0xC0 {
		return 2
	}
	if b&0xF0 == 0xE0 {
		return 3
	}
	if b&0xF8 == 0xF0 {
		return 4
	}
	return 0
}
package evaluate

import (
	"bytes"
	"io/ioutil"
	"text/template"
)

type Template struct {
	template *template.Template
	model    interface{}
}

func New(name, templateString string, model interface{}) *Template {
	tmpl := &Template{
		template: template.New(name),
		model:    model,
	}

	tmpl.template.Funcs(template.FuncMap{
		"include":    tmpl.include,
		"prefix":     prefix,
		"postfix":    postfix,
		"split":      split,
		"escapeYAML": escapeYAML,
	})

	template.Must(tmpl.template.Parse(templateString))

	return tmpl
}

func (t *Template) Execute() (string, error) {
	buf := bytes.NewBufferString("")
	err := t.template.Execute(buf, t.model)
	return buf.String(), err
}

func (t *Template) include(name string, models ...interface{}) (string, error) {
	content, err := ioutil.ReadFile(name)
	if err != nil {
		return "", err
	}

	model := map[string]interface{}{}
	model["parent"] = t.model

	for i := 0; i < len(models); i += 2 {
		model[models[i].(string)] = models[i+1]
	}

	return New(name, string(content), model).Execute()
}

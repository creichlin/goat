# include
{{ range .list -}}
A {{.}}
B {{include "tests/part.tpl" "part" . }}
{{ end }}
# split{{ range (.breaks | split) }}
<   {{.}}   >{{ end }}

# prefix
{{ .breaks | prefix ">>"}}

# postfix
{{ .breaks | postfix ">>"}}

# escape yaml
{{ .breaks | escapeYAML }}
{{ .quotes | escapeYAML }}
{{ .unicode | escapeYAML }}

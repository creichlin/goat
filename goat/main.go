package main

import (
	"log"
	"gitlab.com/creichlin/goat/reader"
	"gitlab.com/creichlin/goat/evaluate"
	"flag"
	"io/ioutil"
	"fmt"
)


type config struct {
	template string
}


func flags() *config {
	help := false
	conf := &config{}

	flag.BoolVar(&help, "help", false, "print help")
	flag.StringVar(&conf.template, "template", "", "name of template file")

	flag.Parse()

	if help {
		flag.PrintDefaults()
		return nil
	}
	return conf
}




func main() {
	conf := flags()
	if conf == nil {
		return
	}

	templateData, err := ioutil.ReadFile(conf.template)
	if err != nil {
		log.Fatalf("Could not read template '%v', %v", conf.template, err)
	}


	model, err := reader.ReadDataFromStdin()
	if err != nil {
		log.Fatal(err)
	}

	template := evaluate.New("root", string(templateData), model)

	result, err := template.Execute()
	if err != nil {
		log.Fatalf("Could not evaluate template %v, %v", conf.template, err)
	}

	fmt.Print(result)
}

package reader

import (
	"os"
	"bytes"
	"gopkg.in/yaml.v2"
	"encoding/json"
	"fmt"
)

func ReadDataFromStdin() (interface{}, error) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(os.Stdin)

	var result interface{}

	erry := yaml.Unmarshal(buf.Bytes(), &result)
	if erry == nil {
		return result, nil
	}

	errj := json.Unmarshal(buf.Bytes(), &result)
	if errj == nil {
		return result, nil
	}

	return nil, fmt.Errorf("Could not unmarshal stdin as yaml or json. %v, %v", erry, errj)
}